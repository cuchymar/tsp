package cz.esw.profiling.ga;

/**
 * Operation combining two parents into a single offspring
 *
 * @author Marek Cuchý
 */
public interface Crossover<T extends Individual> {

	public T perform(T parent1, T parent2);
}
