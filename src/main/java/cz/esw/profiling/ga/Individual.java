package cz.esw.profiling.ga;

/**
 * @author Marek Cuchý
 */
public interface Individual{

	public double objective();
}
