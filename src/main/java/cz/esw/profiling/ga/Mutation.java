package cz.esw.profiling.ga;

/**
 * @author Marek Cuchý
 */
public interface Mutation<T extends Individual> {

	public T mutate(T origin);

}
